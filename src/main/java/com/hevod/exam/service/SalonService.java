package com.hevod.exam.service;

import com.hevod.exam.dto.salon.SalonResponse;
import com.hevod.exam.dto.salon.SalonSaveRequest;
import com.hevod.exam.dto.salon.SalonSaveResponse;
import com.hevod.exam.entity.Salon;
import com.hevod.exam.repository.SalonRepository;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SalonService {

    private final SalonRepository salonRepository;
    private final ModelMapper modelMapper = new ModelMapper();

    public SalonSaveResponse save(@Valid SalonSaveRequest request) {
        Salon map = modelMapper.map(request, Salon.class);
        return new SalonSaveResponse(salonRepository.save(map).getId());
    }

    public SalonResponse findById(String id) {
        return salonRepository.findById(id)
                .map(salon -> modelMapper.map(salon, SalonResponse.class))
                .orElseThrow(IllegalArgumentException::new);
    }

    public void delete(String  id) {
        salonRepository.findById(id)
                .ifPresentOrElse(emp -> {
                    salonRepository.deleteById(id);
                }, () -> {
                    throw new IllegalArgumentException();
                });
    }
}
