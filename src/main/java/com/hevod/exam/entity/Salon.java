package com.hevod.exam.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@Document(collection = "Salons")
public class Salon {

    @Id
    private String id = UUID.randomUUID().toString();
    private String name;
    private String address;
    private String type;


    private String openTime;
    private String closeTime;

    private String directorName;

    private String a;
    private String b;
    private String c;

}
