package com.hevod.exam.repository;

import com.hevod.exam.entity.Salon;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalonRepository extends MongoRepository<Salon, String> {
}
