package com.hevod.exam.dto.salon;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class SalonSaveRequest {

    @NotBlank
    private String name;
    @NotBlank
    private String address;
    @NotBlank
    private String type;

    @NotBlank
    private String openTime;
    @NotBlank
    private String closeTime;

    @NotBlank
    private String directorName;

    private String a;
    private String b;
    private String c;

}
