package com.hevod.exam.controller;

import com.hevod.exam.dto.salon.SalonResponse;
import com.hevod.exam.dto.salon.SalonSaveRequest;
import com.hevod.exam.dto.salon.SalonSaveResponse;
import com.hevod.exam.service.SalonService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/salons")
@RequiredArgsConstructor
public class SalonController {

    private final SalonService salonService;

    @PostMapping
    public SalonSaveResponse save(@Valid @RequestBody SalonSaveRequest request) {
        return salonService.save(request);
    }

    @GetMapping("/{id}")
    public SalonResponse find(@PathVariable String id) {
        return salonService.findById(id);
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        salonService.delete(id);
    }


}
