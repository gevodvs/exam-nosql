package com.hevod.exam.dto.salon;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SalonResponse {

    private String name;
    private String address;
    private String type;


    private String openTime;
    private String closeTime;

    private String directorName;

    private String a;
    private String b;
    private String c;

}
