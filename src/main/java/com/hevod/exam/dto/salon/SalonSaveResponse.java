package com.hevod.exam.dto.salon;

public record SalonSaveResponse(String id) {
}
